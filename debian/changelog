r-cran-shiny (1.10.0+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Added patch to skip a test that needs an unavailable R package
    ("coro").

 -- Michael R. Crusoe <crusoe@debian.org>  Wed, 12 Feb 2025 11:53:24 +0100

r-cran-shiny (1.10.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Charles Plessy <plessy@debian.org>  Thu, 30 Jan 2025 10:32:22 +0900

r-cran-shiny (1.9.1+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Charles Plessy <plessy@debian.org>  Fri, 11 Oct 2024 08:41:01 +0900

r-cran-shiny (1.8.1.1+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Sun, 12 May 2024 02:49:33 +0200

r-cran-shiny (1.8.0+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 21 Nov 2023 21:05:12 +0100

r-cran-shiny (1.7.5.1+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 17 Oct 2023 15:01:15 +0200

r-cran-shiny (1.7.5+dfsg-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)
  * Adapt debian/source/lintian-overrides

 -- Andreas Tille <tille@debian.org>  Mon, 28 Aug 2023 15:12:08 +0200

r-cran-shiny (1.7.4.1+dfsg-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)
  * Remove constraints unnecessary since bullseye (oldstable):
    + Build-Depends: Drop versioned constraint on r-cran-fastmap (>= 1.1.0).

 -- Andreas Tille <tille@debian.org>  Fri, 21 Jul 2023 21:48:29 +0200

r-cran-shiny (1.7.4+dfsg-3) unstable; urgency=medium

  * Fix link for normalize.css
    Closes: #1035428

 -- Andreas Tille <tille@debian.org>  Mon, 15 May 2023 10:50:45 +0200

r-cran-shiny (1.7.4+dfsg-2) unstable; urgency=medium

  * closure-compiler fails - simply symlinking uncompressed JS
    Closes: #1031456

 -- Andreas Tille <tille@debian.org>  Tue, 21 Feb 2023 20:34:31 +0100

r-cran-shiny (1.7.4+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 20 Dec 2022 17:10:16 +0100

r-cran-shiny (1.7.3+dfsg-1) unstable; urgency=medium

  * Fix lintian overrides
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Wed, 02 Nov 2022 14:27:05 +0100

r-cran-shiny (1.7.2+dfsg-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)
  * Build-Depends: r-cran-fontawesome

 -- Andreas Tille <tille@debian.org>  Sun, 31 Jul 2022 15:33:02 +0200

r-cran-shiny (1.6.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Add B-D on r-cran-{bslib, ellipsis, lifecycle}
  * Bump Standards-Version to 4.6.1 (no changes needed)

 -- Nilesh Patra <nilesh@debian.org>  Sun, 15 May 2022 18:12:49 +0530

r-cran-shiny (1.5.0+dfsg-2) unstable; urgency=medium

  * Team Upload.
  * Fix location of fonts to fix broken symlinks
    (Closes: #985499, #985392)

 -- Nilesh Patra <nilesh@debian.org>  Sun, 21 Mar 2021 15:02:07 +0530

r-cran-shiny (1.5.0+dfsg-1) unstable; urgency=medium

  [ Andreas Tille ]
  * New upstream version
  * debhelper-compat 13 (routine-update)
  * d/copyright: Proper sequence of paragraphs
  * Test-Depends: r-cran-dygraphs

  [ Dylan Aïssi ]
  * Fix autopkgtest
  * Add missing packages in test-deps
  * Allow failure for reprotest

 -- Andreas Tille <tille@debian.org>  Tue, 22 Sep 2020 15:25:41 +0200

r-cran-shiny (1.4.0.2+dfsg-2) unstable; urgency=medium

  * Set upstream metadata fields: Bug-Submit, Repository, Repository-
    Browse.

 -- Andreas Tille <tille@debian.org>  Mon, 18 May 2020 21:36:41 +0200

r-cran-shiny (1.4.0.2+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Sat, 14 Mar 2020 15:03:16 +0100

r-cran-shiny (1.4.0+dfsg-1) unstable; urgency=medium

  * Versioned Depends libjs-twitter-bootstrap-datepicker (>= 1.3.1+dfsg1-4)
    since the location of the files had changed over time
  * Simplify d/rules
  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.1
  * autopkgtest: s/ADTTMP/AUTOPKGTEST_TMP/g
  * Set upstream metadata fields: Archive, Bug-Database.
  * Fix link to html5shiv.min.js
    Closes: #942472
  * Build-Depends: r-cran-fastmap

 -- Andreas Tille <tille@debian.org>  Fri, 10 Jan 2020 08:43:02 +0100

r-cran-shiny (1.3.2+dfsg-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.3.0
  * rename debian/tests/control.autodep8 to debian/tests/control
  * No need for privacy fix in reactive-graph.html any more
  * Use html5shiv.min.js from node-html5shiv package

 -- Andreas Tille <tille@debian.org>  Wed, 10 Jul 2019 17:53:37 +0200

r-cran-shiny (1.2.0+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 26 Nov 2018 10:07:16 +0100

r-cran-shiny (1.1.0+dfsg-3) unstable; urgency=medium

  * Team upload.
  * Update d/links to reflect change in libjs-twitter-bootstrap-datepicker
      Closes: #913534
  * Add Testsuite: autopkgtest-pkg-r

 -- Dylan Aïssi <daissi@debian.org>  Mon, 19 Nov 2018 07:51:43 +0100

r-cran-shiny (1.1.0+dfsg-2) unstable; urgency=medium

  * Do not (Build-)Depend: libjs-twitter-bootstrap any more
    Closes: #908429
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Mon, 17 Sep 2018 14:02:45 +0200

r-cran-shiny (1.1.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * Point Vcs fields to salsa.debian.org
  * dh-update-R to update Build-Depends
  * New Build-Depends: r-cran-later, r-cran-promises
  * Replace embedded copy of normalize.css by libjs-normalize.css
    Closes: #898790
  * Remove only CSS files from fonts-awesome since some fonts were dropped
    form the package

 -- Andreas Tille <tille@debian.org>  Tue, 05 Jun 2018 11:16:39 +0200

r-cran-shiny (1.0.5+dfsg-4) unstable; urgency=medium

  * Shiny does not show correct menu if fontawesome-webfont is a symlink -
    replace it by file copy instead.
  * Avoid privacy breaching remote JS code in documentation

 -- Andreas Tille <tille@debian.org>  Wed, 17 Jan 2018 13:36:51 +0100

r-cran-shiny (1.0.5+dfsg-3) unstable; urgency=medium

  * Prepare minimized JS in script after downloading uncompressed source
    instead of doing the compression at package build time
  * Standards-Version: 4.1.3
  * debhelper 11
  * Try hard to find a proper selectize.min.js that works without issues.
    Finally fall back to the one shipped with r-cran-shiny.
  * For some very strange reason a symlink leads to unexpected results in
    shiny apps. Thus a real copy of font-awesome.min.css from
    fonts-font-awesome package is created
  * Secure URI in watch file
  * Fix issues with datepicker
  * Do not parse d/changelog

 -- Andreas Tille <tille@debian.org>  Wed, 17 Jan 2018 11:06:03 +0100

r-cran-shiny (1.0.5+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Rebuild for r-api-3.4 transition.
  * Bump to Standards-Version 4.1.1.

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 30 Sep 2017 23:15:28 +0200

r-cran-shiny (1.0.5+dfsg-1) unstable; urgency=medium

  [ Don Armstrong ]
  * Hack for symlinks to twitter-bootstrap and javascript
    Closes: #867963

  [ Andreas Tille ]
  * New upstream version
  * Standards-Version: 4.1.0 (no changes needed)

 -- Andreas Tille <tille@debian.org>  Thu, 14 Sep 2017 13:20:56 +0200

r-cran-shiny (1.0.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * debhelper 10
  * d/watch: version=4

 -- Andreas Tille <tille@debian.org>  Fri, 13 Jan 2017 09:43:24 +0100

r-cran-shiny (0.14.2+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Convert to dh-r
  * Canonical homepage for CRAN
  * Package libjs-twitter-bootstrap-datepicker does not provide minimized
    version any more - simply link to the unminimized one to make sure
    there will be a file found under this name
    Closes: #838006
  * New Build-Depends: r-cran-sourcetools
  * New JS without source: babel-polyfill.min.js: Create script to fetch source
    and exclude non-source from upstream tarball

 -- Andreas Tille <tille@debian.org>  Sat, 12 Nov 2016 08:21:49 +0100

r-cran-shiny (0.13.2+dfsg-2) unstable; urgency=medium

  * Add missing Depends: r-cran-ggplot2

 -- Andreas Tille <tille@debian.org>  Thu, 28 Apr 2016 00:19:22 +0200

r-cran-shiny (0.13.2+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Add missing Depends: r-cran-rcpp

 -- Andreas Tille <tille@debian.org>  Thu, 21 Apr 2016 16:37:48 +0200

r-cran-shiny (0.13.1+dfsg1-1) unstable; urgency=medium

  * We need to keep some files from upstream source
  * Use dh_link instead of dh_linktrees
    Closes: #820879
  * lintian override for false positives

 -- Andreas Tille <tille@debian.org>  Fri, 15 Apr 2016 23:13:35 +0200

r-cran-shiny (0.13.1+dfsg-1) unstable; urgency=low

  * Initial release (Closes: #819363)

 -- Andreas Tille <tille@debian.org>  Sun, 27 Mar 2016 17:30:56 +0200
